package ru.tsc.kirillov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.ILoggerService;
import ru.tsc.kirillov.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public final class LoggerListener implements MessageListener {

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        loggerService.writeLog(textMessage.getText());
    }

}
