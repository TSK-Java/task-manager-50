package ru.tsc.kirillov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataJsonJaxbLoadRequest extends AbstractUserRequest {

    public DataJsonJaxbLoadRequest(@Nullable final String token) {
        super(token);
    }

}
