package ru.tsc.kirillov.tm.exception.field;

public class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Ошибка! ID не задан.");
    }

    public IdEmptyException(final String message) {
        super(message);
    }

}
