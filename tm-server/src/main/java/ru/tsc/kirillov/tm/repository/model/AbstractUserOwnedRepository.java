package ru.tsc.kirillov.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.kirillov.tm.model.AbstractWbsModel;
import ru.tsc.kirillov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedRepository<M extends AbstractWbsModel>
        extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Class<M> clazz, @NotNull final EntityManager entityManager) {
        super(clazz, entityManager);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final User user, @NotNull final String name) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUser(user);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final User user, @NotNull final String name, @NotNull final String description) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUser(user);
        model.setDescription(description);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(
            @Nullable final User user,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final java.util.Date dateBegin,
            @Nullable final java.util.Date dateEnd
    ) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUser(user);
        model.setDescription(description);
        model.setDateBegin(dateBegin);
        model.setDateEnd(dateEnd);
        return add(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        removeAll(findAll(userId));
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        @NotNull final String jpql = String.format("FROM %s m WHERE m.user.id = :userid", getModelName());
        return entityManager.createQuery(jpql, clazz)
                .setParameter("userid", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.user.id = :userId ORDER BY m.%s",
                getModelName(),
                getColumnSort(comparator)
        );
        return entityManager.createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return false;
        @NotNull final String jpql = String.format(
                "SELECT COUNT(m) = 1 FROM %s m WHERE m.user.id = :userId AND m.id = :id",
                getModelName()
        );
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.user.id = :userId AND m.id = :id",
                getModelName()
        );
        @NotNull final TypedQuery<M> query = entityManager.createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("id", id);
        return getResult(query);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final String jpql = String.format("FROM %s m WHERE m.user.id = :userId", getModelName());
        @NotNull final TypedQuery<M> query = entityManager.createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setFirstResult(index);
        return getResult(query);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null) return 0;
        @NotNull final String jpql = String.format(
                "SELECT COUNT(m) FROM %s m WHERE m.user.id = :userId",
                getModelName()
        );
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
