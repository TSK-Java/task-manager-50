package ru.tsc.kirillov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.UserDTO;
import ru.tsc.kirillov.tm.dto.request.UserProfileRequest;
import ru.tsc.kirillov.tm.dto.response.UserProfileResponse;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "view-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение профиля пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение профиля пользователя]");
        @NotNull UserProfileResponse response = getUserEndpoint().viewProfileUser(new UserProfileRequest(getToken()));
        @Nullable final UserDTO user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("Логин: " + user.getLogin());
        System.out.println("Имя: " + user.getFirstName());
        System.out.println("Фамилия: " + user.getLastName());
        System.out.println("Отчество: " + user.getMiddleName());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Роль: " + user.getRole());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
