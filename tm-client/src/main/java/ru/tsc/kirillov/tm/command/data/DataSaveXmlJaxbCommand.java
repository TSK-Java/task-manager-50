package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.DataXmlJaxbSaveRequest;
import ru.tsc.kirillov.tm.enumerated.Role;

public final class DataSaveXmlJaxbCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-jaxb";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения из xml файла (JAXB API)";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Сохранение состояния приложения из xml файла (JAXB API)]");
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlJaxbSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
