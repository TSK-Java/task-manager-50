package ru.tsc.kirillov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.UserLogoutRequest;
import ru.tsc.kirillov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Выход из пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Выход]");
        getServiceLocator().getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
